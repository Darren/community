section: user research
---
section_id: user-research
---
color: primary
---
_template: layout.html
---
title: Tor Personas
---
subtitle: Personas help us to drive human-centered design processes across teams. Meet our archetypes of Tor users.
---
key: 4
---
html: two-columns-page.html
---
body:

Persona is a tool that represents the needs, thoughts, and goals of the target user. We created personas because they help us to drive human-centered design processes.
As part of our global south travels during 2018 and 2019, we got the lucky chance to meet a lot of different Tor users: from activists to journalists, all of them with different motivations, but demanding a usable private and secure tool to access the Internet.
With the Community Team, we have been working collecting and mapping real user stories and finding patterns across them. It is how our Personas emerged from our in field research.

### Tor Personas
* Jelani, the human rights defender: [View on Gitlab](https://gitlab.torproject.org/tpo/ux/research/-/blob/master/personas/jelani.pdf) · [Download PDF](https://gitlab.torproject.org/tpo/ux/research/-/raw/master/personas/jelani.pdf)
* Aleisha, the privacy seeker: [View on Gitlab](https://gitlab.torproject.org/tpo/ux/research/-/blob/master/personas/aleisha.pdf) · [Download PDF](https://gitlab.torproject.org/tpo/ux/research/-/raw/master/personas/aleisha.pdf)
* Fernanda, the feminist activist: [View on Gitlab](https://gitlab.torproject.org/tpo/ux/research/-/blob/master/personas/fernanda.pdf) · [Download PDF](https://gitlab.torproject.org/tpo/ux/research/-/raw/master/personas/fernanda.pdf)
* Fatima, the censored user: [View on Gitlab](https://gitlab.torproject.org/tpo/ux/research/-/blob/master/personas/fatima.pdf) · [Download PDF](https://gitlab.torproject.org/tpo/ux/research/-/raw/master/personas/fatima.pdf)
* Alex, the fearless journalist: [View on Gitlab](https://gitlab.torproject.org/tpo/ux/research/-/blob/master/personas/alex.pdf) · [Download PDF](https://gitlab.torproject.org/tpo/ux/research/-/raw/master/personas/alex.pdf)
